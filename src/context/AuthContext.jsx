import { useContext, createContext, useEffect, useState } from "react";
import {
  GoogleAuthProvider,
  signInWithPopup,
  signInWithRedirect,
  signOut,
  onAuthStateChanged,
} from "firebase/auth";
import { auth } from "../firebase";
import { loginSSO } from "../apis/authentication";
import { get } from "lodash";

const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [errMessage, setErrMessage] = useState('');

  const googleLogIn = async () => {
    try {
      const provider = new GoogleAuthProvider();
      provider.setCustomParameters({ prompt: "select_account" });
      const { user } = await signInWithPopup(auth, provider);

      const fireBaseAccessToken = user.accessToken;

      const data = await loginSSO(fireBaseAccessToken)
      const accessToken = get(data, ['data', 'accessToken'], '')
      setErrMessage('')
      localStorage.setItem("idToken", accessToken);
      localStorage.setItem('user', JSON.stringify(data));
      setUser(user);
    } catch (error) {
      console.log(error)
    }

  };

  const logOut = () => {
    signOut(auth);
    setUser(null);
    localStorage.removeItem('user');
    localStorage.removeItem("idToken");
    
  };

  return (
    <AuthContext.Provider value={{ googleLogIn, logOut, user, errMessage }}>
      {children}
    </AuthContext.Provider>
  );
};

export const UserAuth = () => {
  return useContext(AuthContext);
};
