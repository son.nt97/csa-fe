export const arrayBufferToBase64 = (arrayBuffer) => {
  const CHUNK_SIZE = 8192; // 8KB chunks
  const uint8Array = new Uint8Array(arrayBuffer);
  const totalChunks = Math.ceil(uint8Array.length / CHUNK_SIZE);
  let base64String = "";

  for (let i = 0; i < totalChunks; i++) {
    const chunkStart = i * CHUNK_SIZE;
    const chunkEnd = Math.min(chunkStart + CHUNK_SIZE, uint8Array.length);
    const chunk = uint8Array.slice(chunkStart, chunkEnd);
    const chunkBase64 = btoa(String.fromCharCode.apply(null, chunk));
    base64String += chunkBase64;
  }

  return base64String;
};
