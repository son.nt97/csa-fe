import React, { useState } from "react";

import { AuthContextProvider } from "./context/AuthContext";
import { Route, Routes, Navigate } from "react-router-dom";

import ProtectedRoute from "./components/ProtectedRoute";
import LoginPage from "./pages/login-page/LoginPage";
import DashboardPage from "./pages/dashboard-page/DashboardPage";
import CandidatesPage from "./pages/candidate-page/CandidatesPage";
import PageNotFound from "./pages/PageNotFound";
import CandidateForm from "./pages/candidate-page/candidate-form/CandidateForm";
import CandidateDetail from "./pages/candidate-page/candidate-detail/CandidateDetail";
import AssessmentResult from "./pages/assessment-page/assessement-result/AssessmentResult";
import AssessmentForm from "./pages/assessment-page/assessment-form/AssessmentForm";
import AssessmentPage from "./pages/assessment-page/AssessmentPage";
import SuccessPage from "./pages/assessment-page/SuccessPage";

function App() {
  return (
    <AuthContextProvider>
      <Routes>
        <Route exact path="/" element={<Navigate to="/candidates" />} />
        <Route path="/login" element={<LoginPage />} />
        <Route
          path="/dashboard"
          element={
            <ProtectedRoute>
              <DashboardPage />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/candidates"
          element={
            <ProtectedRoute>
              <CandidatesPage />
            </ProtectedRoute>
          }
        />
        <Route
          path="/candidates/create"
          element={
            <ProtectedRoute>
              <CandidateForm />
            </ProtectedRoute>
          }
        />
        <Route
          path="/candidates/edit/:candidateId"
          element={
            <ProtectedRoute>
              <CandidateForm />
            </ProtectedRoute>
          }
        />
        <Route
          exact
          path="/candidates/:candidateId"
          element={
            <ProtectedRoute>
              <CandidateDetail />
            </ProtectedRoute>
          }
        />
        {/* 👇️ only match this when no other routes match */}
        <Route
          path="/qualification-test"
          element={<AssessmentPage />}
        />
        <Route
          path="/qualification-test/start-assessment"
          element={<AssessmentForm />}
        />
        <Route
          path="/qualification-test/start-assessment/assessment-finish"
          element={<SuccessPage />}
        />
        <Route
          path="/qualification-test/result"
          element={
            <ProtectedRoute>
              <AssessmentResult />
            </ProtectedRoute>
          }
        />

        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </AuthContextProvider>
  );
}

export default App;
