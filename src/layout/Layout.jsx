import React, { useState } from 'react';

import Sidebar from "../components/Sidebar/Sidebar";

import classes from './Layout.module.css';

function Layout({ children }) {
  return (
    <div className={classes.Layout}>
      <Sidebar />
      <div className={classes.Content}>{children}</div>
    </div>
  );
}

export default Layout;