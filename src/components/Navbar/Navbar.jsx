import React from "react";
import { Link } from "react-router-dom";
import { UserAuth } from "../../context/AuthContext";

import classes from "./Navbar.module.css";
import { Button } from "antd";

const Navbar = () => {
  const { user, logOut } = UserAuth();

  const handleSignOut = async () => {
    try {
      await logOut();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className={classes.Navbar}>
      <h1 className="text-center text-2xl font-bold">HBG</h1>
      {user?.displayName && (
        <Button type="link" onClick={handleSignOut}>
          Log out
        </Button>
      )}
    </div>
  );
};

export default Navbar;
