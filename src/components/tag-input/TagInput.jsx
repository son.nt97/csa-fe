import React, { useState } from 'react';
import { Input, Tag } from 'antd';

const TagInput = () => {
	const [inputValue, setInputValue] = useState('');
	const [tags, setTags] = useState([]);

	const handleInputChange = (e) => {
		setInputValue(e.target.value);
	};

	const handleInputConfirm = () => {
		if (inputValue && !tags.includes(inputValue)) {
			setTags([...tags, inputValue]);
		}
		setInputValue('');
	};

	const handleTagClose = (removedTag) => {
		setTags(tags.filter((tag) => tag !== removedTag));
	};

	return (
		<div>
			{tags.map((tag) => (
				<Tag key={tag} closable onClose={() => handleTagClose(tag)}>
					{tag}
				</Tag>
			))}
			<Input
				type="text"
				value={inputValue}
				onChange={handleInputChange}
				onBlur={handleInputConfirm}
				onPressEnter={handleInputConfirm}
				style={{ width: 'auto' }}
			/>
		</div>
	);
};

export default TagInput;
