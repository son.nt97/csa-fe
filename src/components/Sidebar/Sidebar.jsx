import { faCircleUser, faSignOut } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Divider } from 'antd';
import React from "react";
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { UserAuth } from "../../context/AuthContext";
import classes from "./Sidebar.module.css";

const Sidebar = () => {
  const { logOut } = UserAuth();
  const user = JSON.parse(localStorage.getItem('user')).data.me;
  
  const navigate = useNavigate()
  const location = useLocation();

  const handleSignOut = async () => {
    try {
      await logOut();
      navigate('/login')
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <nav className={classes.Sidebar}>
      <div>
        <div className={classes.ImageText}>
          <img src='/logo.png' alt="" height='50px' width='auto' />
        </div>
        <Divider style={{ margin: 0 }} />
        <div className={classes.MenuBar}>
          <div className={classes.Menu}>
            <ul className={classes.navList}>
              {/* <li className={location.pathname.includes('/dashboard') ? classes.active : ''}>
                <FontAwesomeIcon icon={faListSquares} />
                <Link to="/dashboard">Dashboard</Link>
              </li> */}
              <li className={location.pathname.includes('/candidates') ? classes.active : ''}>
                <FontAwesomeIcon icon={faCircleUser} />
                <Link to="/candidates">Candidates</Link>
              </li>
              {/* <li className={location.pathname === '/contact' ? classes.active : ''}>
              <Link to="/contact">Contact</Link>
            </li> */}
            </ul>
          </div>
          {
            !!user &&
            <div>
              <img className={classes.UserPhoto} src={user.avatar} alt="user photo" />
              <p className={classes.UserName}>{user.username}</p>
              <p className={classes.UserEmail}>{user.email}</p>
              <p className={classes.UserEmail}>{user.role}</p>
            </div>
          }
        </div>
      </div>

      <div className={classes.BottomContent}>
        <Button type="link" onClick={handleSignOut}>
          <FontAwesomeIcon icon={faSignOut} />
          <span style={{ marginLeft: '5px' }}>Log out</span>
        </Button>
      </div>
    </nav>
  );
};

export default Sidebar;
