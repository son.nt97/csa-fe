import React from "react";
import { Navigate } from "react-router-dom";

import Layout from "../layout/Layout";

const ProtectedRoute = ({ children }) => {
  const idToken = localStorage.getItem("idToken");
  const notLogin = !idToken;

  if (notLogin) {
    return <Navigate to={"/login"} />;
  }

  return <Layout>{children}</Layout>;
};

export default ProtectedRoute;
