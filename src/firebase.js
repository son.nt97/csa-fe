import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth } from "firebase/auth";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyASyHi7Ntp-EPSEdcRPA4GyimfzwJsPgyc",
  authDomain: "fcm-demo-2233e.firebaseapp.com",
  projectId: "fcm-demo-2233e",
  storageBucket: "fcm-demo-2233e.appspot.com",
  messagingSenderId: "776137149599",
  appId: "1:776137149599:web:bd1cde505b8e17e73f66aa",
  measurementId: "G-QZZ25XWBQE",
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const auth = getAuth(app);
