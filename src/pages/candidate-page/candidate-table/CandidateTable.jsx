import { Button, Input, Select, Space, Table, Tag } from "antd";
import { DeleteOutlined, EyeOutlined } from "@ant-design/icons";
import React, { useState, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import { getCandidates } from "../../../apis/candidate";

import classes from "./CandidateTable.module.css";
import { get } from "lodash";

const { Option } = Select;

const statusOptions = [
  { label: "Processing", value: "PROCESSING" },
  { label: "Processing Failed", value: "PROCESSING_FAILED" },
  { label: "Question Created", value: "QUESTION_CREATED" },
  { label: "Questions Reviewed", value: "QUESTIONS_REVIEWED" },
  { label: "Link Sent", value: "LINK_SENT" },
  { label: "Doing Test", value: "DOING_TEST" },
  { label: "Completed Test", value: "COMPLETED_TEST" },
  { label: "Scored", value: "SCORED" },
];

export const colorPalette = {
  "PROCESSING": "green",
  "PROCESSING_FAILED": "red",
  "QUESTION_CREATED": "orange",
  "QUESTIONS_REVIEWED": "blue",
  "LINK_SENT": "purple",
  "DOING_TEST": "cyan",
  "COMPLETED_TEST": "magenta",
  "SCORED": "lime",
}

const colorPaletteForResultStatus = {
  "PASSED": "green",
  "FAILED": "red",
  "WAIT_LIST": "orange",
}

const dataSource = [
  { id: 1, name: 'John Doe', age: 28, city: 'New York' },
  { id: 2, name: 'Jane Smith', age: 35, city: 'Los Angeles' },
  { id: 3, name: 'Bob Johnson', age: 42, city: 'Chicago' },
];

const columns = [
  { title: 'ID', dataIndex: 'id', key: 'id' },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Age', dataIndex: 'age', key: 'age' },
  { title: 'City', dataIndex: 'city', key: 'city' },
];

function CandidateTable() {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const [selectionType, setSelectionType] = useState("checkbox");
  const [candidateData, setCandidateData] = useState();

  const [searchText, setSearchText] = useState('');
  const [selectedTags, setSelectedTags] = useState([]);
  const [selectedStatuses, setSelectedStatuses] = useState([]);

  const handleSearch = (e) => {
    setSearchText(e.target.value);
  };

  const handleTagChange = (value) => {
    setSelectedTags(value);
  };

  const handleStatusChange = (value) => {
    setSelectedStatuses(value);
  };

  function handleUpdateCandidate(candidateId) {
    navigate(`${pathname}/${candidateId}`, { state: { candidateId: candidateId } });
  }

  const handleOnclickDetail = ((id) => {
    handleUpdateCandidate(id);
  });

  const columns = [
    {
      title: "Candidate Name",
      dataIndex: "candidateName",
      key: "candidateName",
      width: 160,
    },
    {
      title: "Candidate Email",
      dataIndex: "candidateEmail",
      key: "candidateEmail",
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Tags",
      dataIndex: "tags",
      key: "tags",
      render: (_, { tags }) => (
        <>
          {tags.map((tag) => {
            let color = 'green';
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (_, { status }) => {
        return (<Tag color={colorPalette[status]} key={status}>
          {status.toUpperCase()}
        </Tag>)
      },
    },
    {
      title: "Result Status",
      dataIndex: "resultStatus",
      key: "resultStatus",
      render: (_, { resultStatus }) => {
        if (resultStatus) {
          return (<Tag color={colorPaletteForResultStatus[resultStatus]} key={resultStatus}>
            {resultStatus?.toUpperCase()}
          </Tag>)
        }
        return ''
      },
      width: 130
    },
    {
      title: '',
      dataIndex: "action",
      key: 'action',
      render: (text, record) => (
        <Button onClick={(e) => handleOnclickDetail(record?.id)} icon={<EyeOutlined />} />
      ),
    },
  ];

  useEffect(() => {
    const params = {
      page: 1,
      pageSize: 20,
      content: null,
      status: 1,
    };

    getCandidates(params).then((res) => {
      setCandidateData(res);
    });
  }, []);

  const filteredData = get(candidateData, 'data', []).filter((item) => {
    // Filter by search text
    const candidateEmail = (item?.candidateEmail || '').toLowerCase();
    const candidateName = get(item, 'candidateName', '').toLowerCase();
    const searchTextLower = searchText.toLowerCase();
    const nameMatch = searchText === '' || candidateName.trim().includes(searchTextLower.trim()) || candidateEmail.trim().includes(searchTextLower.trim());

    // Filter by selected ages
    const status = item.status;
    const statusMatch = selectedStatuses.length === 0 || selectedStatuses.includes(status.trim());

    // Filter by selected cities
    const tags = item.tags.map((tag) => tag.toLowerCase().trim());
    const isSubarray = selectedTags.every((value) => tags.includes(value.toLowerCase().trim()));
    const tagMatch = selectedTags.length === 0 || isSubarray;

    // console.log({
    //   tags,
    //   selectedTags,
    //   isSubarray,
    //   tagMatch,
    // })

    return nameMatch && statusMatch && tagMatch;
  });

  const tagList = get(candidateData, 'data', []).map((item) => {
    return item.tags;
  });

  const uniqueTagOptions = tagList.flat().filter((item, index, self) => self.indexOf(item) === index);

  return (
    <div>
      <div className={classes.Filters}>
        <Input placeholder="Search candidate name or email" value={searchText} onChange={handleSearch} style={{ width: 350, marginRight: 16 }} />
        <Select
          mode="multiple"
          placeholder="Select Tags"
          value={selectedTags}
          onChange={handleTagChange}
          style={{ width: 200, marginRight: 16 }}
        >
          {
            uniqueTagOptions.map((tag, i) => (
              <Option key={i} value={tag}>{tag}</Option>
            ))
          }
        </Select>
        <Select
          mode="multiple"
          placeholder="Select Statuses"
          value={selectedStatuses}
          onChange={handleStatusChange}
          style={{ width: 400, marginRight: 16 }}
        >
          {
            statusOptions.map(({ label, value }, i) => (
              <Option key={i} value={value}>{label}</Option>
            ))
          }
        </Select>
      </div>
      <Table
        rowKey={(data) => data.id}
        className={classes.CandidateTable}
        dataSource={filteredData}
        columns={columns}
      />
    </div>
  );
}

export default CandidateTable;
