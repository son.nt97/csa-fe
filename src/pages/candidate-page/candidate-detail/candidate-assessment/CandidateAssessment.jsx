import React from "react";
import { Tabs, Card, Typography, Button, Tag, message } from "antd";
import { get } from "lodash";
import { useParams } from "react-router-dom";

import { startScoringManually } from "../../../../apis/candidate";

import classes from "./CandidateAssessment.module.css";

const { Title, Paragraph } = Typography;

const CandidateAssessment = ({ data }) => {
  const { candidateId } = useParams();

  const questionList = get(data, ["gptQuestion", "questions"], []);
  const answers = get(data, ["candidateAnswer", "answers"], []);
  const candidateAssessments = get(data, "candidateAssessments", []);
  const status = get(data, "status", "");

  let updatedQuestions = questionList.reduce((acc, item) => {
    const questions = item.questions.map((questionItem) => {
      return {
        ...questionItem,
        group: item.group,
        answer: "",
      };
    });
    return [...acc, ...questions];
  }, []);

  answers.map((answer) => {
    const answerQuestionId = get(answer, [0, "questionId"], "");
    const answerContent = get(answer, [0, "answer"], "");
    // find the question in the questions array with the matching id
    const question = updatedQuestions.find((q) => q.id === answerQuestionId);
    // set the answer property of the matching question to the answer string
    if (question) {
      question.answer = answerContent;
    }
  });

  const questionsWithAnswersAndScores = updatedQuestions.map((question) => {
    const matchingAssessment = candidateAssessments.find(
      (assessment) => assessment.questionId === question.id
    );
    if (matchingAssessment) {
      return {
        ...question,
        score: matchingAssessment.score.total_score,
        comment: matchingAssessment.score.comment,
      };
    } else {
      return question;
    }
  });

  const startScoring = async () => {
    try {
      await startScoringManually(candidateId);

      message.success("Manually trigger score test successfully!");
    } catch (error) {
      console.log({ error });
    }
  };

  if (answers && answers.length > 0) {
    return (
      <div className={classes.CandidateAssessment}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            <span>Candidate Testing Status: </span>
            <Tag color="#f50">{status}</Tag>
          </div>
          {status === "COMPLETED_TEST" && (
            <Button
              type="primary"
              onClick={startScoring}
              style={{ width: "180px", height: "50px" }}
            >
              Start Scoring
            </Button>
          )}
        </div>

        {questionsWithAnswersAndScores.map((item) => (
          <Card className="m-2">
            <div style={{ marginBottom: "25px" }}>
              <Tag color="#2db7f5">{item.group}</Tag>
              {item?.skill && <Tag color="magenta">{item.skill}</Tag>}
            </div>
            <h5>{item.question}</h5>
            <Paragraph>{item.answer}</Paragraph>
            {(item.score || item.score === 0) && (
              <p style={{ color: "red", fontWeight: "600" }}>
                Score: {item.score}
              </p>
            )}
            {item.comment && (
              <p style={{ color: "green", fontWeight: "600" }}>
                ChatGPT comment: {item.comment}
              </p>
            )}
          </Card>
        ))}
      </div>
    );
  } else {
    return <i>No assessment can be found for this candidate!</i>;
  }
};

export default CandidateAssessment;
