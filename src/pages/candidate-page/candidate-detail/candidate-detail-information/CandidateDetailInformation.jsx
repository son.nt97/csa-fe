import React from "react";
import classes from "./CandidateDetailInformation.module.css";
import { Card, Row, Col, Typography, Space, Tag, Avatar, Button } from "antd";
const { Text } = Typography;
const { Meta } = Card;
import { colorPalette } from "../../candidate-table/CandidateTable";
import { downloadPdf } from "../../../../apis/candidate";
import { get } from "lodash";

const CandidateDetailInformation = ({ data }) => {
  const cardStyle = {
    width: '100%',
    marginTop: "10px",
  };
  const handleDownload = async () => {
    const response = await downloadPdf(data?.fileKeys);
    if (response) {
      const blob = new Blob([response], {
        type: 'application/pdf',
      });
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = data?.fileKeys;
      link.click();
    }
  };

  const position = get(data, ['gptReview','review', 'position'], '')
  const level = get(data, ['gptReview','review', 'level'], '')

  console.log('--- data ag', data);

  return (
    <div
      className={classes.CandidateDetailInformation}
    >
      <div className="container mx-0">
        <Row justify="left">
          <Col xs={24}>
            <Card title={data?.title} bordered={false}>
              <Text type="secondary">{data?.description}</Text>
              <Row>
                <Col span={8}>
                  <Text strong>Name:</Text>
                </Col>
                <Col span={16}>
                  <Text>{data?.candidateName}</Text>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Text strong>Email:</Text>
                </Col>
                <Col span={16}>
                  <Text> {data?.candidateEmail}</Text>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Text strong>Question language:</Text>
                </Col>
                <Col span={16}>
                <Tag color="blue">{data?.language}</Tag>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Text strong>Status:</Text>
                </Col>
                <Col span={16}>
                <Tag color={colorPalette[data?.status]}> {data?.status}</Tag>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Text strong>Tags:</Text>
                </Col>
                <Col span={16}>
                  <Space size={[0, 8]} wrap>
                    {data?.tags.map((option, idx) => (
                      <Tag key={idx} color="green">{option}</Tag>
                    ))}
                  </Space>
                </Col>
              </Row>
            <Button onClick={handleDownload}>Download CV</Button>
            </Card>
          </Col>
        </Row>
      </div>

      {/* GPT Review */}
      <div className="container mx-0">
        <Row justify="left">
          <Col xs={24}>
            <Card title='GPT Review' bordered={false}>
              <Row>
                <Col span={8}>
                  <Text strong>Position:</Text>
                </Col>
                <Col span={16}>
                <Text>{position}</Text>
                </Col>
              </Row>
              <Row>
                <Col span={8}>
                  <Text strong>Level:</Text>
                </Col>
                <Col span={16}>
                <Tag>{level}</Tag>
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
      </div>

      {/* Related users */}

      <div className="container m-0">
        <Row>
          <Col xs={24}>
            <Card title="Related User" bordered={false}>
              {data?.userFollowCandidates.map((option, idx) => (
                  <div key={idx} xs={24}>
                    <Card style={cardStyle}>
                      <Meta
                        avatar={<img className={classes.UserAvatar} src={option?.user.avatar} alt="avatar" />}
                        title={option?.user.username}
                        description={
                          <div>
                            <div style={{ marginBottom: "8px" }}>
                              {option?.user.email}
                            </div>
                            <Tag color="red">{option?.user.role}</Tag>
                          </div>
                        }
                      />
                    </Card>
                  </div>
              ))}
            </Card>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default CandidateDetailInformation;
