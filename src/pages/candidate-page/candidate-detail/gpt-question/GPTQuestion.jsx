import React, { useState } from "react";
import { useParams } from "react-router";
import { get } from "lodash";
import { Tabs, Card, Typography, Button, message } from "antd";

import { reviewQuestions, sendLinkTest } from "../../../../apis/candidate";

import classes from "./GPTQuestion.module.css";

const { Title, Paragraph } = Typography;
const { TabPane } = Tabs;
const GPTQuestion = ({ data }) => {
  const { candidateId } = useParams();
  const [status, setStatus] = useState(get(data, "status", ""));

  const handleReview = async () => {
    try {
      await reviewQuestions(candidateId);

      message.success("Update assessment's status to reviewed successfully!");

      setStatus("QUESTIONS_REVIEWED");
    } catch (error) {
      console.log(error);
    }
  };

  const handleSendLinkTest = async () => {
    try {
      await sendLinkTest(candidateId);

      message.success("Send link test successfully!");
    } catch (error) {
      console.log(error);
    }
  };

  if (status && status !== "PROCESSING" && status !== "PROCESSING_FAILED") {
    const { gptQuestion } = data;

    return (
      <div className={classes.GPTQuestion}>
        <div className="m-2">
          {status === "QUESTION_CREATED" && (
            <Button
              type="primary"
              onClick={handleReview}
              style={{ width: "180px", height: "50px" }}
            >
              Review
            </Button>
          )}
          {status === "QUESTIONS_REVIEWED" && (
            <Button
              type="primary"
              onClick={handleSendLinkTest}
              style={{ width: "180px", height: "50px" }}
            >
              Send Link Test
            </Button>
          )}
        </div>
        <Tabs defaultActiveKey="1">
          {gptQuestion?.questions.map((option, index) => (
            <TabPane tab={option.group} key={index + 1}>
              {option?.questions.map((quiz, quizIndex) => (
                <Card className="m-2">
                  <Title level={4}>{quiz.skill}</Title>
                  <Paragraph>
                    <span>
                      Question {quizIndex + 1}: {quiz.question}
                    </span>
                  </Paragraph>
                </Card>
              ))}
            </TabPane>
          ))}
        </Tabs>
      </div>
    );
  } else {
    return <i>No question can be found for this candidate!</i>;
  }
};

export default GPTQuestion;
