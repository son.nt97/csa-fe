import React from "react";
import { get } from "lodash";

import RadarChart from "./RadarChart";

import classes from "./CandidateAssessmentResult.module.css";

const CandidateAssessmentResult = ({ data }) => {
  const candidateAssessments = get(data, "candidateAssessments", []);
  const questionList = get(data, ["gptQuestion", "questions"], []);

  const updatedQuestions = questionList.reduce((acc, item) => {
    const questions = item.questions.map((questionItem) => {
      return {
        ...questionItem,
        skill: questionItem.skill || item.group,
        group: item.group,
        answer: "",
      };
    });
    return [...acc, ...questions];
  }, []);

  const radarChartData = updatedQuestions.map((question) => {
    const matchingAssessment = candidateAssessments.find(
      (assessment) => assessment.questionId === question.id
    );
    if (matchingAssessment) {
      return {
        score: matchingAssessment.score.total_score,
        skill: question.skill,
      };
    } else {
      return question;
    }
  });

  const uniqueSkills = radarChartData.reduce((acc, cur) => {
    const existingSkillIndex = acc.findIndex((item) => item.skill === cur.skill);

    if (existingSkillIndex === -1) {
      acc.push(cur);
    } else {
      const existingSkill = acc[existingSkillIndex];
      const existingScore = existingSkill.score;
      const newScore = Math.floor((existingScore + cur.score) / 2);
      acc[existingSkillIndex] = { skill: cur.skill, score: newScore };
    }

    return acc;
  }, []);


  const status = get(data, "status", "");

  if (status === "SCORED") {
    return (
      <div className={classes.CandidateAssessmentResult}>
        <div className={classes.Chart}>
          <RadarChart data={uniqueSkills} />
        </div>
      </div>
    );
  } else {
    return <i>Assessment has not scored yet!</i>;
  }
};

export default CandidateAssessmentResult;
