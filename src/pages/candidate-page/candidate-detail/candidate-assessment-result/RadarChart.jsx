import React, { useEffect, useRef } from "react";
import Chart from "chart.js/auto";

const RadarChart = ({ data }) => {
  console.log("radar chart");
  console.log({ data });
  const chartContainer = useRef(null);

  useEffect(() => {
    if (chartContainer && chartContainer.current) {
      // Define data labels and values
      const labels = data.map((item) => item.skill);
      const values = data.map((item) => item.score);

      // Define chart data
      const chartData = {
        labels: labels,
        datasets: [
          {
            label: "Scores",
            data: values,
            backgroundColor: "rgba(255, 99, 132, 0.2)",
            borderColor: "rgba(255, 99, 132, 1)",
            borderWidth: 2,
          },
        ],
      };

      // Define chart options
      const chartOptions = {
        scale: {
          r: {
            max: 10,
            min: 0,
            ticks: {
              stepSize: 1,
            },
          },
        },
        width: 1000,
        height: 800,
        maintainAspectRatio: false,
      };

      // Create chart instance
      const chartInstance = new Chart(chartContainer.current, {
        type: "radar",
        data: chartData,
        options: chartOptions,
      });

      return () => {
        chartInstance.destroy();
      };
    }
  }, [data]);

  return <canvas ref={chartContainer} />;
};

export default RadarChart;
