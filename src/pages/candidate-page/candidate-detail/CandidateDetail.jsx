import React, { useEffect, useState } from "react";
import { Button, Spin, Tabs } from "antd";

import classes from "./CandidateDetail.module.css";
import { useLocation, useNavigate, useParams } from "react-router-dom";

import { getCandidateDetail } from "../../../apis/candidate";

import GPTQuestion from "./gpt-question/GPTQuestion";
import CandidateAssessment from "./candidate-assessment/CandidateAssessment";
import CandidateAssessmentResult from "./candidate-assessment-result/CandidateAssessmentResult";
import CandidateDetailInformation from "./candidate-detail-information/CandidateDetailInformation";

const { TabPane } = Tabs;

const CandidateDetail = () => {
  const { candidateId } = useParams();
  const navigate = useNavigate();

  const [candidateDetail, setCandidateDetail] = useState(null);
  const [loading, setLoading] = useState(false);
  const [activeTab, setActiveTab] = useState("1");

  const handleTabChange = (key) => {
    setActiveTab(key);
  };

  useEffect(() => {
    setLoading(true);
    getCandidateDetail(candidateId)
      .then((res) => {
        setCandidateDetail(res?.data);
      })
      .catch((err) => {
        console.log({ err });
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const goBack = () => {
    navigate(-1);
  };

  // if (loading) {
  //   return <Spin />;
  // }

  if (candidateDetail) {
    const {
      candidateAnswer,
      candidateAssessments,
      gptQuestion,
      gptReview,
      candidateEmail,
      candidateName,
      description,
      title,
      fileKeys,
      tags,
      language,
      userFollowCandidates,
      status,
    } = candidateDetail;

    const candidateDetailInformation = {
      candidateEmail,
      candidateName,
      description,
      title,
      fileKeys,
      tags,
      language,
      userFollowCandidates,
      gptQuestion,
      candidateAnswer,
      status,
      gptReview
    };

    const candidateAssessmentResultData = {
      candidateAssessments,
      gptQuestion,
      gptReview,
      status,
    };

    const candidateAssessmentData = {
      candidateAnswer,
      gptQuestion,
      candidateAssessments,
      status,
    };

    const gptQuestionData = {
      gptQuestion,
      status,
    };

    const items = [
      {
        key: "1",
        label: `Candidate Information`,
        children: (
          <CandidateDetailInformation data={candidateDetailInformation} />
        ),
      },
      {
        key: "2",
        label: `GPT Question`,
        children: <GPTQuestion data={gptQuestionData} />,
      },
      {
        key: "3",
        label: `Assessment`,
        children: <CandidateAssessment data={candidateAssessmentData} />,
      },
      {
        key: "4",
        label: `Skills Matrix`,
        children: (
          <CandidateAssessmentResult data={candidateAssessmentResultData} />
        ),
      },
    ];

    return (
      <div className={classes.CandidateDetail}>
        <div className={classes.Header}>
          <Button type="link" onClick={goBack}>
            Back
          </Button>
          <h1 className={classes.Title}>Candidate Detail</h1>
        </div>
        <div>
          <Tabs
            defaultActiveKey="1"
            items={items}
            activeKey={activeTab}
            onChange={handleTabChange}
          />
        </div>
      </div>
    );
  }
};

export default CandidateDetail;
