import React from "react";
import { Button } from "antd";
import { useLocation, useNavigate } from "react-router-dom";

import classes from "./CandidatesPage.module.css";
import CandidateTable from "./candidate-table/CandidateTable";

const CandidatesPage = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  function handleAddCandidate() {
    navigate(`${pathname}/create`);
  }

  return (
    <div className={classes.CandidatesPage}>
      <div className={classes.Header}>
        <h1>Candidates</h1>
        <Button onClick={handleAddCandidate}>Add Candidate</Button>
      </div>
      {/* <Table data */}
      <CandidateTable />
    </div>
  );
};

export default CandidatesPage;
