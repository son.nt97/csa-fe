import { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { Form, Input, Button, message, Upload, Select, Radio } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import TagsInput from "react-tagsinput";
import "react-tagsinput/react-tagsinput.css";

import classes from "./CandidateForm.module.css";
import { get } from "lodash";
import { useNavigate } from "react-router-dom";
import { createCandidate } from "../../../apis/candidate";
import { getUsers } from "../../../apis/user";

const { Dragger } = Upload;
const { Option } = Select;

function CandidateForm() {
  const navigate = useNavigate();

  const [pdfContent, setPdfContent] = useState("");
  const [fileList, setFileList] = useState([]);
  const [usersData, setUsers] = useState(null);

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
    getValues,
  } = useForm({
    defaultValues: {
      title: "",
      description: "",
      relatedUsers: [],
      tags: [],
      candidateName: "",
      candidateEmail: "",
      language: "",
      file: null,
    },
  });

  const formData = getValues();
  const cvFileName = get(formData, ["file", "file", "name"], "");

  const onSubmit = (data) => {
    message.success("Form submitted successfully");

    const { title, description, relatedUsers, tags, candidateEmail, candidateName, language } = data;
    console.log('----------- data --', data);
    const blob = new Blob([pdfContent]);

    const candidateInfo = {
      title,
      description,
      relatedUsers,
      tags,
      file: blob,
      candidateName,
      candidateEmail,
      language,
      // cv: pdfContent,
    };

    reset();
    setPdfContent("");

    createCandidate(candidateInfo)
      .then((res) => {
        console.log(res);
        const candidateId = get(res, ["data", "id"], '');
        if (candidateId) {
          navigate(`/candidates/${candidateId}`);
        }
      })
      .catch((err) => console.log(err));
  };

  const validatePdfFile = (file) => {
    console.log({ file });
    const { fileList } = file;
    if (fileList.length === 0) {
      return "Please select a file";
    }
    if (fileList[0].type !== "application/pdf") {
      return "File must be a PDF";
    }
    return true;
  };

  const beforeUpload = async (file) => {
    const reader = new FileReader();
    reader.onload = async (event) => {
      // Do something with the PDF data
      setPdfContent(event.target.result);
    };
    reader.readAsArrayBuffer(file);
    return false; // Prevent upload
  };

  const goBack = () => {
    navigate(-1);
  };

  useEffect(() => {
    const params = {
      page: 1,
      pageSize: 999,
      content: null,
      status: 1,
    };

    getUsers(params).then((res) => {
      setUsers(res);
    });
  }, []);

  return (
    <div className={classes.CandidateForm}>
      <div className={classes.Header}>
        <Button type="link" onClick={goBack}>
          Back
        </Button>
        <h1 className={classes.Title}>Add Candidate</h1>
      </div>

      <Form layout="vertical" onFinish={handleSubmit(onSubmit)}>
        <Form.Item label="Candidate Name " required>
          <Controller
            name="candidateName"
            control={control}
            initialValues=""
            render={({ field }) => <Input {...field} />}
            rules={{
              required: "Please enter a name",
              minLength: {
                value: 6,
                message: "Username must be at least 6 characters long",
              },
            }}
          />
          {errors.candidateName && <p className={classes.ErrorMessage}>{errors.candidateName.message}</p>}
        </Form.Item>
        <Form.Item label="Candidate Email" required>
          <Controller
            name="candidateEmail"
            control={control}
            initialValues=""
            render={({ field }) => <Input {...field} />}
            rules={{
              required: "Please enter an email",
              pattern: {
                value: /\S+@\S+\.\S+/,
                message: "Please enter a valid email",
              },
            }}
          />
          {errors.candidateEmail && <p className={classes.ErrorMessage}>{errors.candidateEmail.message}</p>}
        </Form.Item>
        <Form.Item label="Title" required>
          <div>
            <Controller
              name="title"
              control={control}
              initialValues=""
              render={({ field }) => <Input {...field} />}
              rules={{
                required: "Please enter a title",
                // minLength: {
                //   value: 6,
                //   message: "Username must be at least 6 characters long",
                // },
              }}
            />
          </div>
          {errors.title && <p className={classes.ErrorMessage}>{errors.title.message}</p>}
        </Form.Item>
        <Form.Item label="Description">
          <Controller
            name="description"
            control={control}
            initialValues=""
            render={({ field }) => <Input {...field} />}
          />
          {errors.description && <p className={classes.ErrorMessage}>{errors.description.message}</p>}
        </Form.Item>
        <Form.Item label="CV (PDF only)" required>
          <Controller
            name="file"
            control={control}
            render={({ field }) => (
              <div className={classes.CustomDragger}>
                <Dragger
                  {...field}
                  accept=".pdf"
                  beforeUpload={beforeUpload}
                  fileList={fileList}
                  multiple={false}
                >
                  <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag PDF file to this area to upload
                  </p>
                </Dragger>
              </div>
            )}
            rules={{
              required: "Please upload a PDF file",
              validate: validatePdfFile,
            }}
          />
          {cvFileName && (
            <>
              <span>Selected File:</span>
              <span> {cvFileName}</span>
            </>
          )}
          {errors.file && <p className={classes.ErrorMessage}>{errors.file.message}</p>}
        </Form.Item>
        <Form.Item label="Related Users" required>
          <Controller
            name="relatedUsers"
            control={control}
            render={({ field }) => (
              <Select id="relatedUsers" mode="multiple" {...field}>
                {usersData?.data.map((option) => (
                  <Option key={option.id} value={option.id}>
                    {option.username}
                  </Option>
                ))}
              </Select>
            )}
          />
          {errors.relatedUsers && <p className={classes.ErrorMessage}>{errors.relatedUsers.message}</p>}
        </Form.Item>
        <Form.Item label="Tags">
          <Controller
            name="tags"
            control={control}
            render={({ field }) => (
              <TagsInput
                id="tags"
                value={field.value}
                onChange={(tags) => field.onChange(tags)}
              />
            )}
          />
          {/* {errors.tags && <p>{errors.tags.message}</p>} */}
        </Form.Item>
        <Form.Item label="Questions language">
          <Controller
            name="language"
            control={control}
            render={({ field }) => (
              <Radio.Group
                id="language"
                value={field.value}
                onChange={(language) => field.onChange(language)}
              >
                <Radio value="English">English</Radio>
                <Radio value="Vietnamese">Vietnamese</Radio>
                <Radio value="Japanese">Japanese</Radio>

              </Radio.Group>
            )}
          />
          {/* {errors.tags && <p>{errors.tags.message}</p>} */}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
          <Button onClick={reset} style={{ marginLeft: "10px" }}>
            Reset
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default CandidateForm;
