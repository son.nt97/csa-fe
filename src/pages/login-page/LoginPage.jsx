import React, { useEffect, useState } from "react";
import { GoogleButton } from "react-google-button";
import { Navigate, useNavigate } from "react-router-dom";
import { Form, Input, Button, Checkbox } from "antd";
import { Controller, useForm } from "react-hook-form";

import { UserAuth } from "../../context/AuthContext";

import classes from "./LoginPage.module.css";

const LoginPage = () => {
  const { googleLogIn, user, errMessage } = UserAuth();

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const onSubmit = (data) => {
    console.log(data);
  };

  const handleGoogleLogin = async () => {
    try {
      await googleLogIn();
    } catch (error) {
      console.log("error", error);
    }
  };

  const isLoggedIn = user && Object.keys(user).length !== 0;

  if (isLoggedIn) {
    return <Navigate to="/candidates" />;
  }

  return (
    <div className={classes.LoginPageBackground}>
      <div className={classes.LoginPage}>
        <h1 className={classes.LoginTitle}>Log in</h1>
        <Form
          className={classes.LoginForm}
          layout="vertical"
          onFinish={handleSubmit(onSubmit)}
        >
          <Form.Item label="Username" required>
            <Controller
              name="username"
              control={control}
              initialValues=""
              render={({ field }) => <Input {...field} />}
              rules={{
                required: "Please enter a username",
                minLength: {
                  value: 6,
                  message: "Username must be at least 6 characters long",
                },
              }}
            />
            {errors.username && <p className={classes.ErrorMessage}>{errors.username.message}</p>}
          </Form.Item>
          <Form.Item label="Password" required>
            <Controller
              name="password"
              control={control}
              initialValues=""
              render={({ field }) => <Input {...field} />}
              rules={{
                required: "Please enter an password",
                pattern: {
                  value: /^[a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]+\.)?hblab\.vn$/,
                  message: "Please enter a valid password",
                },
              }}
            />
            {errors.password && <p className={classes.ErrorMessage}>{errors.password.message}</p>}
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className={classes.submitButton}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>

        {errMessage && <p className={classes.ErrorMessage}>{errMessage}</p>}

        <div className={classes.line}></div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <GoogleButton onClick={handleGoogleLogin} />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
