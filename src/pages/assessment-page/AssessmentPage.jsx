import React, { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { Button } from "antd";
import { get } from "lodash";

import classes from "./AssessmentPage.module.css";
import { getAssessmentInfo } from "../../apis/assessment";

const AssessmentPage = () => {
  const navigate = useNavigate();
  const { pathname, search } = useLocation();

  const queryParams = new URLSearchParams(search);
  const linkTestToken = queryParams.get("token") || "";

  const [assessmentInfo, setAssessmentInfo] = useState(null);

  const totalTimeInSec = get(assessmentInfo, 'totalTimeInSec', 0)
  const numOfQuestions = get(assessmentInfo, 'numOfQuestions', 0)


  function formatTime(time) {
    let sec = time;
    let hours = Math.floor(sec / 3600); // get hours
    let minutes = Math.floor((sec - hours * 3600) / 60); // get minutes
    let seconds = sec - hours * 3600 - minutes * 60; //  get seconds
    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }

    return `${hours}:${minutes}:${seconds}`;
  }

  function goToAssessment() {
    navigate(`${pathname}/start-assessment?token=${linkTestToken}`);
  }

  useEffect(() => {
    getAssessmentInfo(linkTestToken).then((res) => {
      const data = get(res, 'data', {});

      setAssessmentInfo(data);
    });
  }, []);

  if (assessmentInfo) {
    return (
      <div className={classes.AssessmentPage}>
        <div className={classes.Title}>Assessment</div>
        <div className={classes.desc}>
          <p>Welcome <span style={{ fontWeight: '700', fontSize: '20px', color: '#1677ff' }}>{get(assessmentInfo, 'candidateName', '')}</span> to your assessment!</p>
          <p>We're excited that you've taken the time to apply for this position and we appreciate your interest in our company.
            Before you begin, we want to provide you with some information about the assessment. The assessment is designed to evaluate your skills and qualifications for the position you've applied for. It consists of a series of questions and tasks that will test your knowledge and abilities in relevant areas.</p>
          <p> We encourage you to take your time and carefully read each question before answering. Make sure you understand what is being asked of you and provide thoughtful, well-reasoned responses. There is no time limit for the assessment, but we recommend that you complete it in one sitting if possible.</p>
        </div>

        <div className={classes.assessmentDetailsBlocks}>
          <div className={classes.border}>
            <div className={classes.value}>{formatTime(totalTimeInSec)}</div>
            <div>Overall Time</div>
          </div>
          <div className={classes.border}>
            <div className={classes.value}>{numOfQuestions}</div>
            <div>Total Questions</div>
          </div>
        </div>
        <Button
          type="primary"
          className={classes.StartButton}
          onClick={goToAssessment}
        >
          Start Assessment
        </Button>
      </div>
    );
  }
};

export default AssessmentPage;
