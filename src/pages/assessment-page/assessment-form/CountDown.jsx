import React, { useState, useEffect } from "react";
import { useForm, useFormContext } from "react-hook-form";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStopwatch } from "@fortawesome/free-solid-svg-icons";

import classes from "./Countdown.module.css";

const Countdown = ({ initialTime, onSubmit }) => {
  const { getValues } = useFormContext();

  const [countdownTime, setCountdownTime] = useState(initialTime);

  useEffect(() => {
    const interval = setInterval(() => {
      setCountdownTime((prevTime) => prevTime - 1);
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (countdownTime === 0) {
      const formData = getValues();
      onSubmit(formData);
    }
  }, [countdownTime, onSubmit]);

  const formatTime = (time) => {
    const hours = Math.floor(time / 3600)
      .toString()
      .padStart(2, "0");
    const minutes = Math.floor((time % 3600) / 60)
      .toString()
      .padStart(2, "0");
    const seconds = (time % 60).toString().padStart(2, "0");
    return `${hours}:${minutes}:${seconds}`;
  };

  return (
    <div className={classes.Countdown}>
      <FontAwesomeIcon icon={faStopwatch} />
      <span style={{ marginLeft: "5px" }}>
        {formatTime(countdownTime >= 0 ? countdownTime : 0)}
      </span>
    </div>
  );
};

export default Countdown;
