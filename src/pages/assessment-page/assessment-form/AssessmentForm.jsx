import React, { useRef, useEffect, useCallback, useState } from "react";
import {
  useForm,
  Controller,
  useFormContext,
  FormProvider,
} from "react-hook-form";
import { useLocation, useNavigate } from "react-router-dom";
import { Button, Checkbox, Form, Input, Radio } from "antd";
import { get } from "lodash";

import Countdown from "./CountDown";
import {
  finishAssessment,
  startAssessment,
  submitAssessmentAnswers,
} from "../../../apis/assessment";

import classes from "./AssessmentForm.module.css";

const AssessmentForm = () => {
  const navigator = useNavigate();
  const { pathname, search } = useLocation();

  const queryParams = new URLSearchParams(search);
  const linkTestToken = queryParams.get("token") || "";
  const methods = useForm();

  const [assessmentData, setAssessmentData] = useState(null);
  const [answerList, setAnswerList] = useState([]);
  const [isSubmitAnswer, setIsSubmitAnswer] = useState(false);

  const remainingTime = get(assessmentData, "timeRemainingInSec", 0);
  const questionList = get(assessmentData, "questions", []);

  const updatedQuestions = questionList.reduce((acc, item) => {
    const questions = item.questions.map((questionItem) => {
      return {
        ...questionItem,
        group: item.group,
        answer: "",
      };
    });
    return [...acc, ...questions];
  }, []);

  const [currentQuestionId, setCurrentQuestionId] = useState(0);

  const onSubmit = async () => {
    try {
      await submitAnswer();

      await finishAssessment(linkTestToken);
      navigator(`${pathname}/assessment-finish`);
    } catch (error) {
      alert(`Failed to submit quiz: ${error.message}`);
    }
  };

  const onForceSubmit = async (data) => {
    console.log({ data });
    // postAssessmentAnswers(data)
    //   .then((response) => {
    //     navigator(`${pathname}/assessment-finish`);
    //   })
    //   .catch((err) => {
    //     alert(`Failed to submit quiz: ${err.message}`);
    //   });
  };

  const submitAnswer = async () => {
    const questionId = get(updatedQuestions[currentQuestionId], "id", "");

    const newAnswer = [
      {
        questionId: questionId,
        answer: get(methods.getValues().question, [questionId], ""),
      },
    ];

    setAnswerList([...answerList, newAnswer]);

    setIsSubmitAnswer(true);
  };

  useEffect(() => {
    if (isSubmitAnswer) {
      const answerPayload = {
        linkTestToken,
        answers: answerList,
      };

      submitAssessmentAnswers(answerPayload)
        .then((res) => {
          console.log({ res });
          setIsSubmitAnswer(false);
        })
        .catch((err) => {
          console.log({ err });
          setIsSubmitAnswer(false);
        });
    }
  }, [isSubmitAnswer]);

  const goBack = () => {
    setCurrentQuestionId(currentQuestionId - 1);
  };

  const goNext = () => {
    setCurrentQuestionId(currentQuestionId + 1);

    submitAnswer();
  };

  useEffect(() => {
    // get question list for testing
    startAssessment(linkTestToken)
      .then((res) => {
        const data = get(res, "data", null);

        if (data) {
          setAssessmentData(data);
        }
      })
      .catch((err) => console.log(err));
  }, []);

  if (updatedQuestions.length > 0) {
    const {
      id,
      question,
      type = "textInput",
      options,
    } = updatedQuestions[currentQuestionId];

    return (
      <div className={classes.AssessmentForm}>
        <FormProvider {...methods}>
          <div className={classes.CountdownClock}>
            <Countdown initialTime={remainingTime} onSubmit={onForceSubmit} />
          </div>
          <Form
            className={classes.Form}
            layout="vertical"
            // onSubmit={methods.handleSubmit(onSubmit)}
          >
            <div className={classes.Question} key={id}>
              <h3>{question}</h3>
              <Form.Item key={id}>
                <Controller
                  name={`question[${id}]`}
                  control={methods.control}
                  defaultValue=""
                  // rules={{ required: true }}
                  render={({ field }) => {
                    switch (type) {
                      case "single-choice":
                        return (
                          <Radio.Group {...field}>
                            {options.map((option) => (
                              <Radio.Button
                                key={option.id}
                                value={option.text}
                                // style={{ display: "block", marginBottom: "8px" }}
                              >
                                {option.text}
                              </Radio.Button>
                            ))}
                          </Radio.Group>
                        );
                      case "multiple-choice":
                        return (
                          <div>
                            <Checkbox.Group {...field}>
                              {options.map((option) => (
                                <Checkbox
                                  key={option.id}
                                  value={option.text}
                                  // style={{ display: "block", marginBottom: "8px" }}
                                >
                                  {option.text}
                                </Checkbox>
                              ))}
                            </Checkbox.Group>
                          </div>
                        );
                      case "textInput":
                        return (
                          <Input.TextArea
                            {...field}
                            placeholder="Enter your question"
                            autoSize={{ minRows: 7, maxRows: 10 }}
                          />
                        );
                      default:
                        return null;
                    }
                  }}
                />
              </Form.Item>
            </div>
            <div className={classes.ActionContainer}>
              {currentQuestionId > 0 && (
                <Button
                  className={classes.SubmitButton}
                  // type="primary"
                  onClick={goBack}
                >
                  Back
                </Button>
              )}
              {currentQuestionId >= 0 &&
                currentQuestionId < updatedQuestions.length - 1 && (
                  <Button
                    className={classes.SubmitButton}
                    // type="primary"
                    onClick={goNext}
                  >
                    Next
                  </Button>
                )}
              {currentQuestionId === updatedQuestions.length - 1 && (
                <Button
                  className={classes.SubmitButton}
                  type="primary"
                  onClick={onSubmit}
                  // htmlType="submit"
                >
                  Submit
                </Button>
              )}
            </div>
          </Form>
        </FormProvider>
      </div>
    );
  }
};

export default AssessmentForm;
