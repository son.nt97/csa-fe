import React from "react";

import classes from "./SuccessPage.module.css";

const SuccessPage = () => {
  return (
    <div className={classes.SuccessPage}>
      <div>🎉 Congrats, you did it. Let's evaluate.</div>
      <p>
        You can close assessment mode, sit back and relax while we evaluate your
        submission.
      </p>
    </div>
  );
};

export default SuccessPage;
