import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";

const PageNotFound = () => {
  return (
    <div style={{ textAlign: 'center' }}>
      <h1>
        <FontAwesomeIcon icon={faExclamationTriangle} /> 404 Page Not Found
      </h1>
      <p>The page you are looking for does not exist.</p>
    </div>
  );
};

export default PageNotFound;
