import { message } from "antd";
import axios from "axios";

const instance = axios.create({
  baseURL: "http://52.76.104.184:8000",
});

instance.interceptors.request.use((config) => {
  const token = localStorage.getItem("idToken");
  if (token) {
    config.headers["Authorization"] = `Bearer ${token}`;
  }
  return config;
});

// Add a response interceptor
instance.interceptors.response.use(
  response => {
    // Return the response data
    return response.data;
  },
  error => {
    // Handle any errors
    if(error.response.status === 401) {
      localStorage.removeItem("idToken");
      window.location.href = "/login";
    }else if(error.response.status === 403 || error.response.status === 404 || error.response.status === 400 ||  error.response.status === 500) {
      message.error(error.response?.data?.message || 'Something went wrong');
    }
    else {
      return Promise.reject(error);
    }

  }
);

export default instance;
