import api from "./api.js";

export const getCandidateDetail = (candidateId) => {
  return api.get(`/csa/api/v1/candidate/${candidateId}`);
};

export const getCandidates = (params) => {
  return api
    .get("/csa/api/v1/candidate", {
      params,
    })
    .catch((err) => console.log(err));
};

export const createCandidate = (candidateData) => {
  const bodyFormData = new FormData();
  for (const [key, value] of Object.entries(candidateData)) {
    console.log(`${key}: ${value}`);
    bodyFormData.append(key, value);
  }

  return api.post("/csa/api/v1/candidate", bodyFormData, {
    headers: {
      // Set the Content-Type header to 'multipart/form-data' along with the boundary
      "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`,
    },
  });
};

export const updateCandidate = (candidateId, candidateData) => {
  return api.put(`/candidates/${candidateId}`, candidateData);
};

export const deleteCandidate = (candidateId) => {
  return api.delete(`/candidates/${candidateId}`);
};

export const reviewQuestions = (candidateId) => {
  const bodyFormData = new FormData();

  return api.post(
    `/csa/api/v1/candidate/${candidateId}/review-questions`,
    {},
    {
      headers: {
        // Set the Content-Type header to 'multipart/form-data' along with the boundary
        "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`,
      },
    }
  );
};

export const sendLinkTest = (candidateId) => {
  const bodyFormData = new FormData();

  return api.post(
    `/csa/api/v1/candidate/${candidateId}/send-link-test`,
    {},
    {
      headers: {
        // Set the Content-Type header to 'multipart/form-data' along with the boundary
        "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`,
      },
    }
  );
};

export const downloadPdf = async (fileKeys) => {
  return api.get(`/csa/api/v1/candidate/cv/${fileKeys}`, {
    headers: {
      "Content-Type": "application/pdf",
    },
    responseType: 'blob',
  })
  .catch((err) => console.log(err));
};

export const startScoringManually = (candidateId) => {
  const bodyFormData = new FormData();

  return api.post(
    `/csa/api/v1/chat-gpt-assistant/trigger-score-test/${candidateId}`,
    {},
    {
      headers: {
        // Set the Content-Type header to 'multipart/form-data' along with the boundary
        "Content-Type": `multipart/form-data; boundary=${bodyFormData._boundary}`,
      },
    }
  );
};
