import api from "./api.js";

export const startAssessment = (linkTestToken) => {
  return api.post("/csa/api/v1/do-test/start", { linkTestToken });
};

export const getAssessmentInfo = (linkTestToken) => {
  return api.post("/csa/api/v1/do-test/overview-info", { linkTestToken });
};

export const finishAssessment = (linkTestToken) => {
  return api.post("/csa/api/v1/do-test/submit", { linkTestToken });
};

export const submitAssessmentAnswers = (assessemntAnswers) => {
  return api.post("/csa/api/v1/do-test/answer", assessemntAnswers);
};
