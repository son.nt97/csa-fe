import api from "./api.js";

export const getUsers = (params) => {
    
    return api.get("/csa/api/v1/user/find-all", {
        headers: {
          "accept": `*/*`,
        },
        params
      })
      .catch((err) => console.log(err));
};
  