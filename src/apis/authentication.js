import api from "./api.js";

export const login = (email, password) => {
  return api.post("/login", { email, password }).then((response) => {
    const token = response.data.token;
    setJwtToken(token);
    return decodeToken(token);
  });
};

export const loginSSO = (idToken) => {
  return api
    .post("/csa/api/v1/auth/login-sso", { idToken })
    .then((response) => {
      return response;
    });
};

export const logout = () => {
  setJwtToken(null);
};

export const getLoggedInUser = () => {
  const token = getJwtToken();
  if (token) {
    return decodeToken(token);
  } else {
    return null;
  }
};

export const setJwtToken = (token) => {
  if (token) {
    document.cookie = `jwtToken=${token}; Secure; HttpOnly; SameSite=Strict`;
  } else {
    document.cookie =
      "jwtToken=; Secure; HttpOnly; SameSite=Strict; expires=Thu, 01 Jan 1970 00:00:00 GMT";
  }
};

export const getJwtToken = () => {
  const cookies = document.cookie.split("; ");
  for (const cookie of cookies) {
    const [name, value] = cookie.split("=");
    if (name === "jwtToken") {
      return value;
    }
  }
  return null;
};

export const decodeToken = (token) => {
  try {
    const decoded = JSON.parse(atob(token.split(".")[1]));
    return {
      userId: decoded.userId,
      email: decoded.email,
      // ... other user properties
    };
  } catch (error) {
    return null;
  }
};
